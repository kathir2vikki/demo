/*package com.test.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.web.servlet.MockMvc;

import com.test.demo.config.CORSFilter;
import com.test.demo.controller.OrderHistoryController;
import com.test.demo.entity.Investment;
import com.test.demo.model.HistoryDto;
import com.test.demo.model.OrderHistoryDto;
import com.test.demo.model.TransactionDto;
import com.test.demo.reponse.Response;
import com.test.demo.repository.OrderHistoryRepository;
import com.test.demo.reponse.DataResponse;
import com.test.demo.service.impl.OrderServiceImpl;

@WebMvcTest(OrderHistoryController.class)
public class OrderHistoryServiceTest {

	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
    private OrderService orderService;
    
    @InjectMocks
    private OrderHistoryController orderHistoryController;
    
    @BeforeEach
    void init() {
       mockMvc = MockMvcBuilders.standaloneSetup(orderHistoryController).addFilter(new CORSFilter()).build();
   }

    @Test
    void getOrderDetail() {
        Optional<Investment> stockDetail = Optional.of(new Investment());
        when(customerTransactionService.getCustomerTransaction(1l)).thenReturn(dataResponse);
        when(orderHistoryRepository.findByCustomerId(Mockito.any())).thenReturn(stockDetail);
        LocalDate currentDate = LocalDate.parse("2021-05-29");
        Response response = orderServiceImpl.getOrderHistoryById(11L, currentDate);
        assertEquals(HttpStatus.OK, response.getHttpStatus());
      assertEquals("Order Details Found", response.getResponseMessage());
    }  
    
    @Test
    public void getOrderDetails() throws Exception {
    	Map<String, Object> response = new HashMap<>();
        response.put("OrderDetails :",getCustomerTransaction());
        DataResponse dataResponse = new DataResponse(HttpStatus.OK, HttpStatus.OK.value(),
        		"Order Details Found", response);
        LocalDate currentDate = LocalDate.parse("2021-05-29");
        when(orderService.getOrderHistoryById(11l,currentDate)).thenReturn(dataResponse);
        mockMvc.perform(get("/customers/orderhistory/11/2021-05-29").header("Origin", "*").contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk()).andDo(print());

    }
    
    private HistoryDto getCustomerTransaction() {
    	TransactionDto transactionObj1=new TransactionDto();
    	transactionObj1.setTotalPrice(600.00);
    	transactionObj1.setTransactionId(2l);
    	transactionObj1.setStockName("ICICI");
    	
    	TransactionDto transactionObj2=new TransactionDto();
    	transactionObj1.setTotalPrice(2000.00);
    	transactionObj1.setTransactionId(3l);
    	transactionObj1.setStockName("AXIS");
    	
    	List<TransactionDto> transactionList=new ArrayList<>();
    	transactionList.add(transactionObj1);
    	transactionList.add(transactionObj2);
    	
    	OrderHistoryDto OrderHistoryDto = new OrderHistoryDto();
    	OrderHistoryDto.setAccountNumber(3l);
    	OrderHistoryDto.setTransactionDto(transactionList);
    	
    	List<OrderHistoryDto> orderHistoryDtoList=new ArrayList<>();
    	orderHistoryDtoList.add(OrderHistoryDto);
    	HistoryDto historyDto = new HistoryDto();
    	historyDto.setCustomerId(11l);
    	historyDto.setOrderHistoryDto(orderHistoryDtoList);
        return historyDto;
    }
}
*/