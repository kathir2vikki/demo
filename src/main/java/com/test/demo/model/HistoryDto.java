package com.test.demo.model;

import java.util.List;

public class HistoryDto {

		
		private long customerId;
		private List<OrderHistoryDto> orderHistoryDto;
		public long getCustomerId() {
			return customerId;
		}
		public void setCustomerId(long customerId) {
			this.customerId = customerId;
		}
		public List<OrderHistoryDto> getOrderHistoryDto() {
			return orderHistoryDto;
		}
		public void setOrderHistoryDto(List<OrderHistoryDto> orderHistoryDto) {
			this.orderHistoryDto = orderHistoryDto;
		}
		@Override
		public String toString() {
			return "HistoryDto [customerId=" + customerId + ", orderHistoryDto=" + orderHistoryDto + "]";
		}

}
