package com.test.demo.model;

public class TransactionDto {

	public TransactionDto() {
		super();
	}

	public TransactionDto(String stockName, Long transactionId, double totalPrice) {
		super();
		this.stockName = stockName;
		this.transactionId = transactionId;
		this.totalPrice = totalPrice;
	}

	private String stockName;
	private Long transactionId;
    private double totalPrice;

 

    public String getStockName() {
        return stockName;
    }
    
    @Override
	public String toString() {
		return "TransactionDto [stockName=" + stockName + ", transactionId=" + transactionId + ", totalPrice="
				+ totalPrice + "]";
	}

	public void setStockName(String stockName) {
        this.stockName = stockName;
    }

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

}

