package com.test.demo.model;

import java.util.List;

public class OrderHistoryDto {


	private Long accountNumber;
	private List<TransactionDto> transactionDto;

	public List<TransactionDto> getTransactionDto() {
		return transactionDto;
	}

	public void setTransactionDto(List<TransactionDto> transactionDto) {
		this.transactionDto = transactionDto;
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

/*	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}*/


}
