package com.test.demo.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customer_transactions")
public class Transaction {

	@Id
	@Column(name = "transaction_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;
	@Column(name = "instrument_name")
	@NotNull
	@Size(min = 3)
	private String instrumentName;
	@NotNull
	@Size(min = 1)
	@Column(name = "quantity")
	private int quantity;
	@Column(name = "unit_Price")
	@NotNull
	private double unitPrice;
	@Column(name = "transaction_date")
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate  transactionDate;
	@Column(name = "instrument_type")
	private String instrumentType;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "account_number", referencedColumnName = "account_number", nullable = false)
	@JsonIgnore
	private Investment investment;

	public Transaction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getInstrumentName() {
		return instrumentName;
	}

	public void setInstrumentName(String instrumentName) {
		this.instrumentName = instrumentName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public LocalDate  getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(LocalDate  transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getInstrumentType() {
		return instrumentType;
	}

	public void setInstrumentType(String instrumentType) {
		this.instrumentType = instrumentType;
	}

	public Investment getInvestment() {
		return investment;
	}

	public void setInvestment(Investment investment) {
		this.investment = investment;
	}

	@Override
	public String toString() {
		return "Transaction [transactionId=" + transactionId + ", instrumentName=" + instrumentName + ", quantity="
				+ quantity + ", unitPrice=" + unitPrice + ", transactionDate=" + transactionDate + ", instrumentType="
				+ instrumentType + ", investment=" + investment + "]";
	}

}
