package com.test.demo.entity;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "investment_account")
public class Investment {

	@Id
	@Column(name = "account_Number")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long accountNumber;
	
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "customer_id", referencedColumnName = "customer_id", nullable = false)
	@JsonIgnore
	private Customer customer;
	
	
	@Column(name = "account_balance")
	@NotNull
	@Size(min = 1)
	private double accountBalance;
	@Column(name = "account_date")
	private LocalDate accountDate;
	@Column(name = "account_type")
	@NotNull
	@Size(min = 4)
	private String accountType;
	@OneToMany(mappedBy = "investment", cascade = CascadeType.ALL)
	@JsonIgnore
	private List<Transaction> transaction;

	public Investment() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public LocalDate getAccountDate() {
		return accountDate;
	}

	public void setAccountDate(LocalDate accountDate) {
		this.accountDate = accountDate;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public List<Transaction> getTransaction() {
		return transaction;
	}

	public void setTransaction(List<Transaction> transaction) {
		this.transaction = transaction;
	}

}
