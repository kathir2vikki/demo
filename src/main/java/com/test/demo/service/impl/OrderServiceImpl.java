package com.test.demo.service.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.test.demo.entity.Investment;
import com.test.demo.entity.Transaction;
import com.test.demo.exception.RecordNotFoundException;
import com.test.demo.model.HistoryDto;
import com.test.demo.model.OrderHistoryDto;
import com.test.demo.model.TransactionDto;
import com.test.demo.reponse.DataResponse;
import com.test.demo.reponse.Response;
import com.test.demo.repository.OrderHistoryRepository;
import com.test.demo.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderHistoryRepository orderHistoryRepository;

	List<Investment> orderList;
	/*@Override
	public Response getOrderHistoryById(Long customerId, LocalDate todayDate) {

		Optional<Investment> investDtls = orderHistoryRepository.findByCustomerId(customerId);
		
		if (investDtls.isPresent()) {
			orderList = orderHistoryRepository.getOrderHistoryById(customerId, todayDate);
			HistoryDto historyDto = new HistoryDto();
			if (orderList.isEmpty()) {
				throw new RecordNotFoundException(
						"Record not found for Customer Id = " + customerId + "and for todays date = " + todayDate);
			} else {
				List<OrderHistoryDto> orderHistoryDtoList = new ArrayList<>();
				OrderHistoryDto orderHistoryDto = new OrderHistoryDto();
				historyDto.setCustomerId(customerId);
				for (Investment ls : orderList) {

					orderHistoryDto.setAccountNumber(ls.getAccountNumber());

					List<Transaction> transactionObject = ls.getTransaction();
					
					List<TransactionDto> transactionDtoList = new ArrayList<>();

					for (Transaction transaction : transactionObject) {
						TransactionDto transactionDto = new TransactionDto();
						transactionDto.setTransactionId(transaction.getTransactionId());
						transactionDto.setStockName(transaction.getInstrumentName());
						transactionDto.setTotalPrice(transaction.getQuantity() * transaction.getUnitPrice());
						transactionDtoList.add(transactionDto);

					}
					orderHistoryDto.setTransactionDto(transactionDtoList);
					orderHistoryDtoList.add(orderHistoryDto);
					break;
				}
				historyDto.setOrderHistoryDto(orderHistoryDtoList);
				
				Map<String, Object> response = new HashMap<>();
	            response.put("orderDetails", historyDto);
	            
	            return new DataResponse(HttpStatus.OK, HttpStatus.OK.value(), "Order Detail Found", response);
			}
		} else {
			throw new RecordNotFoundException("Record not found for Customer Id = " + customerId);
		}
	}*/
	
	
	@Override
	public Response findByCustomerId(Long customerId)
	{
		Optional<Investment> investDtls = orderHistoryRepository.findByCustomerId(customerId);
		  if (investDtls.isPresent()) {
	            return new DataResponse(HttpStatus.OK, HttpStatus.OK.value(), "Customer Detail Found", null);
	        }
	        return new Response(HttpStatus.NOT_FOUND, HttpStatus.NOT_FOUND.value(), "Customer Detail Not Found");
	}
	
	
	@Override
	public Response getOrderHistoryById(Long customerId, LocalDate todayDate) {

		Optional<Investment> investDtls = orderHistoryRepository.findByCustomerId(customerId);
		System.out.println("kathir");
		if (investDtls.isPresent()) {
			orderList = orderHistoryRepository.getOrderHistoryById(customerId, todayDate);
			HistoryDto historyDto = new HistoryDto();
			if (orderList.isEmpty()) {
				throw new RecordNotFoundException(
						"Record not found for Customer Id = " + customerId + "and for todays date = " + todayDate);
			} else {
				List<OrderHistoryDto> orderHistoryDtoList = new ArrayList<>();
				OrderHistoryDto orderHistoryDto = new OrderHistoryDto();
				historyDto.setCustomerId(customerId);
				for (Investment ls : orderList) {

					orderHistoryDto.setAccountNumber(ls.getAccountNumber());

					List<Transaction> transactionObject = ls.getTransaction();
					
					List<TransactionDto> transactionDtoList = transactionObject.stream().map(action -> new TransactionDto
							(action.getInstrumentName(),action.getTransactionId(),action.getQuantity()*action.getUnitPrice())).collect(Collectors.toList());

					orderHistoryDto.setTransactionDto(transactionDtoList);
					orderHistoryDtoList.add(orderHistoryDto);
					break;
				}
				historyDto.setOrderHistoryDto(orderHistoryDtoList);
				
				Map<String, Object> response = new HashMap<>();
	            response.put("orderDetails", historyDto);
	            
	            return new DataResponse(HttpStatus.OK, HttpStatus.OK.value(), "Order Detail Found", response);
			}
		} else {
			throw new RecordNotFoundException("Record not found for Customer Id = " + customerId);
		}
	}
	

}
