package com.test.demo.service;

import java.time.LocalDate;

import com.test.demo.reponse.Response;

public interface OrderService {
	
	Response getOrderHistoryById(Long customerId, LocalDate todayDate);

	Response findByCustomerId(Long customerId);

}
