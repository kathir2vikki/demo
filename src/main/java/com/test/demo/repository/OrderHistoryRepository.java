package com.test.demo.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.demo.entity.Investment;

@Repository
public interface OrderHistoryRepository extends JpaRepository<Investment, Long> {

	@Query(value = "select i from Investment i where i.customer.customerId = ?1")
	public Optional<Investment> findByCustomerId(Long customerId);
	
	@Query(value = "SELECT * FROM customer_transactions INNER JOIN investment_account ON customer_transactions.account_number=investment_account.account_number and investment_account.customer_id = ?1 and customer_transactions.transaction_date = ?2", 
			nativeQuery = true)
	public List<Investment> getOrderHistoryById(Long customerId, LocalDate todayDate);
	
}
