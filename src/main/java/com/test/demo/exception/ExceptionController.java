package com.test.demo.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.test.demo.controller.OrderHistoryController;


@ControllerAdvice(assignableTypes = {OrderHistoryController.class})
public class ExceptionController {
	
	private String INCORRECT_REQUEST = "INCORRECT_REQUEST";
	private String BAD_REQUEST = "BAD_REQUEST";
	
	@ExceptionHandler({RecordNotFoundException.class})
	public ResponseEntity<ErrorClass> recordNotFound(RecordNotFoundException e)
	{
		List<String> errorList = new ArrayList<>();
		errorList.add(e.getLocalizedMessage());
		ErrorClass errorMessage = new ErrorClass(INCORRECT_REQUEST, errorList);
		return new ResponseEntity<ErrorClass>(errorMessage, HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorClass> checkForValidMethodArg(MethodArgumentNotValidException e)
	{
		List<String> listDesp = new ArrayList<>();
		
		e.getBindingResult().getFieldErrors().forEach(ex -> listDesp.add(ex.getField()+ ":"+ ex.getDefaultMessage()));
		ErrorClass errorClass = new ErrorClass(BAD_REQUEST, listDesp);
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorClass);
	}

}
