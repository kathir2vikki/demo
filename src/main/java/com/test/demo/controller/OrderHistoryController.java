package com.test.demo.controller;

import java.time.LocalDate;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.demo.entity.Investment;
import com.test.demo.reponse.Response;
import com.test.demo.repository.OrderHistoryRepository;
import com.test.demo.service.OrderService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@Validated
@Api(value = "Order History")
@RequestMapping("/customers")
public class OrderHistoryController {

	@Autowired
	private OrderService orderService;

	/*@Autowired
	OrderHistoryRepository orderHistoryRepository;*/

	/**
	 * Get Order details
	 *
	 * @return Order details
	 */

	@ApiOperation(value = "View Order History")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })
	@GetMapping("/orderhistory/{customerId}/{todayDate}")
	public ResponseEntity<Object> getOrderHistory(@PathVariable("customerId") Long customerId,
			@PathVariable("todayDate") String todayDate) {
		LocalDate currentDate = LocalDate.parse(todayDate);
		Response response = orderService.getOrderHistoryById(customerId, currentDate);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

	/**
	 * Check Customer ID is exist or not
	 *
	 * @return Customer is present
	 */
	@ApiOperation(value = "Customer details")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully retrieved list"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found") })

	@GetMapping("/accountInfo")
	public ResponseEntity<Object>  getAccountInfo(@RequestParam @Valid Long customerId) {
		Response response  = orderService.findByCustomerId(customerId);
		return new ResponseEntity<>(response, response.getHttpStatus());
	}

}
