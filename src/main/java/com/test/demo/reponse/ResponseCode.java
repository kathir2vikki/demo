package com.test.demo.reponse;


	public class ResponseCode {
		private ResponseCode() {
			
		}
		public static final int UNAUTHORIZED = 401; 
		public static final int SUCCESS = 200;
		public static final int BADREQUEST = 400; 
		public static final int INTERNALSERVER = 500;
		public static final int CREATED = 201;
		public static final int NOT_FOUND = 404;
	}


