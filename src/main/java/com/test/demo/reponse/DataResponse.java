package com.test.demo.reponse;



	import org.springframework.http.HttpStatus;

	import java.util.ArrayList;
	import java.util.List;
	import java.util.Map;


	public class DataResponse extends Response {

		//Map<String, Object> data = new HashMap<>();
		Object data;
	//	List<InvalidField> trace = new ArrayList<>();

		public DataResponse() {
		}
		
		public DataResponse(int code, String message, Object data) {
			super(code, message);
			this.data = data;
		}

		/*public DataResponse(int code, String message, List<InvalidField> trace) {
			super(code, message);
			this.trace = trace;
		}*/

		public DataResponse(HttpStatus httpstatuscode, int code, String message, Map<String, Object> data) {
			super(httpstatuscode,code, message);
			this.data = data;
		}
		public DataResponse(HttpStatus httpstatuscode,int code, String message, Object data) {
			super(httpstatuscode,code, message);
			this.data = data;
		}
		public static DataResponse buildResponse(String responseMessage, Object object) {
			return new DataResponse(HttpStatus.OK, ResponseCode.SUCCESS,
					responseMessage != null ? responseMessage : "Success", object);
		}
		
		public Object getData() {
			return data;
		}

		public void setData(Map<String, Object> data) {
			this.data = data;
		}

		/*public List<InvalidField> getTrace() {
			return trace;
		}

		public void setTrace(List<InvalidField> trace) {
			this.trace = trace;
		}*/

		

}
